using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstScript : MonoBehaviour
{
  [SerializeField] float _speed;
  private Vector3 _direction;
  private Rigidbody _rigidbody;
 
  void Start()
  {
    _rigidbody = GetComponent<Rigidbody>();
   
  }

  void Update()
  {
    _direction = Vector3.zero;
    _direction.x = Input.GetAxis("Horizontal"); // ������ �����
    _direction.z = Input.GetAxis("Vertical"); // ������ �����
    if (_direction.x == 0 && _direction.z == 0) return;

    Vector3 position = transform.position;
    position.y = 0.5f;
  }

  private void FixedUpdate()
  {
    _rigidbody.AddForce(_direction * _speed, ForceMode.Impulse);
    _rigidbody.velocity = Vector3.zero;
  }
}
